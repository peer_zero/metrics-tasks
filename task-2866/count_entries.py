# list of bridge pool assignment files in chronological order
file_filelist = '2012filelist.txt'

# get list of filepaths
with open(file_filelist, 'r') as f:
	filelist = f.readlines()

print '%s,%s,%s,%s,%s,%s,%s,%s,%s' % ('date', 'email', 'https',
	'unallocated', 'unknown', 'frac_email', 'frac_https', 'frac_unallocated',
	'frac_unknown')

# iterate over assignment files in order
for filepath in filelist:
	filepath = filepath.rstrip('\r\n')

	# open assignment file and discard first two lines (header and date)
	with open(filepath, 'r') as f:
		header_meta = f.readline()
		header_date = f.readline().rstrip('\r\n')
		
		# extract data date
		date_data = header_date.split(' ')
		
		# counts
		email_count = 0
		https_count = 0
		unallocated_count = 0
		unknown_count = 0

		# iterate over assignment records
		for line in f:
			line = line.rstrip('\r\n')
			
			# get pool assignment
			line_data = line.split(' ')
			
			# filter by pool
			if line_data[1] == 'email':
				email_count += 1
			elif line_data[1] == 'https':
				https_count += 1
			elif line_data[1] == 'unallocated':
				unallocated_count += 1
			else:
				unknown_count += 1

		# output data for file, taking into account division by zero
		# date email https unallocated unknown fraction_versions
		sum_count = email_count + https_count + unallocated_count + unknown_count
		if sum_count == 0:
			sum_count += 1
		print '%s %s,%d,%d,%d,%d,%f,%f,%f,%f' % (date_data[1], date_data[2], 
			email_count, https_count, unallocated_count, unknown_count, 
			float(email_count)/float(sum_count), 
			float(https_count)/float(sum_count), 
			float(unallocated_count)/float(sum_count), 
			float(unknown_count)/float(sum_count))

