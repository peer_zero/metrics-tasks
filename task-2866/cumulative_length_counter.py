# list of bridge pool assignment files in chronological order
file_filelist = '2012filelist.txt'
# assignment pool to count
pool = 'unallocated'

# get list of filepaths
with open(file_filelist, 'r') as f:
	filelist = f.readlines()

# dictionaries to store fingerprint and presence counts based on sessions
length_store = {}
current_store = {}

# iterate over assignment files in order
for filepath in filelist:
	filepath = filepath.rstrip('\r\n')

	# open assignment file and discard first two lines (header and date)
	with open(filepath, 'r') as f:
		header_meta = f.readline()
		header_date = f.readline()
		# iterate over assignment records
		for line in f:
			line = line.rstrip('\r\n')

			# get hashed bridge fingerprint and the assigned pool
			line_data = line.split(' ')
			hashed_bridge_fp = line_data[0]
			allocation = line_data[1]
		
			# filter by assignment pool
			if allocation != pool:
				continue

			# process bridge if it has not been seen before in this file
			if hashed_bridge_fp not in current_store:
				current_store[hashed_bridge_fp] = 1
				# add bridge entry if it has not been active or seen before
				if hashed_bridge_fp not in length_store:
					length_store[hashed_bridge_fp] = 0
				# increment assignment file presence counter
				length_store[hashed_bridge_fp] += 1

	# replace
	current_store = {}

# output presence count entries
for hashed_bridge_fp in length_store:
	print(length_store[hashed_bridge_fp])

